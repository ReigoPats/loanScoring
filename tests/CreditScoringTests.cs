using loanScoring.LoanService;
using loanScoring.Credit;

namespace LoanScoringTest
{
    public class CreditScoringTests
    {
        private LoanService _loanService;
        [SetUp]
        public void Setup()
        {
            _loanService = new LoanService();
        }

        [Test]
        public void Get_Scoring_For_In_Dept_Person()
        {
            LoanApplication loanApplication = new LoanApplication();
            loanApplication.LoanAmount = 2000;
            loanApplication.LoanPeriod = 12;
            loanApplication.PersonalCode = "49002010965";
            CreditScore cs = _loanService.ApplyForLoan(loanApplication);

            CreditScore expectedCreditScore = new CreditScore();
            expectedCreditScore.MaxLoanAmount = 0;
            expectedCreditScore.Score = 0;


            Assert.AreEqual(12, cs.LoanPeriod);
            Assert.AreEqual(0, cs.MaxLoanAmount);
            Assert.AreEqual(0, cs.Score);
            Assert.AreEqual(false, cs.CanIssueLoan);
        }


        [Test]
        public void Get_Scoring_Offer_Higher_Period()
        {
            LoanApplication loanApplication = new LoanApplication();
            loanApplication.LoanAmount = 2000;
            loanApplication.LoanPeriod = 12;
            loanApplication.PersonalCode = "49002010976";
            CreditScore cs = _loanService.ApplyForLoan(loanApplication);

            CreditScore expectedCreditScore = new CreditScore();
            expectedCreditScore.MaxLoanAmount = 0;
            expectedCreditScore.Score = 0;

            Assert.AreEqual(20, cs.LoanPeriod);
            Assert.AreEqual(2000, cs.MaxLoanAmount);
            Assert.AreEqual(true, cs.CanIssueLoan);
        }

        [Test]
        public void Get_Scoring_Offer_Lower_amount()
        {
            LoanApplication loanApplication = new LoanApplication();
            loanApplication.LoanAmount = 4000;
            loanApplication.LoanPeriod = 20;
            loanApplication.PersonalCode = "49002010976";
            CreditScore cs = _loanService.ApplyForLoan(loanApplication);

            CreditScore expectedCreditScore = new CreditScore();
            expectedCreditScore.MaxLoanAmount = 0;
            expectedCreditScore.Score = 0;

            Assert.AreEqual(20, cs.LoanPeriod);
            Assert.AreEqual(2000, cs.MaxLoanAmount);
            Assert.AreEqual(true, cs.CanIssueLoan);
        }

        [Test]
        public void Get_Scoring_Offer_Higher_Amount()
        {
            LoanApplication loanApplication = new LoanApplication();
            loanApplication.LoanAmount = 2000;
            loanApplication.LoanPeriod = 12;
            loanApplication.PersonalCode = "49002010987";
            CreditScore cs = _loanService.ApplyForLoan(loanApplication);

            CreditScore expectedCreditScore = new CreditScore();
            expectedCreditScore.MaxLoanAmount = 0;
            expectedCreditScore.Score = 0;

            Assert.AreEqual(12, cs.LoanPeriod);
            Assert.AreEqual(3600, cs.MaxLoanAmount);
            Assert.AreEqual(true, cs.CanIssueLoan);
        }

        [Test]
        public void Get_Scoring_For_Higher_Amount_Over_Limit()
        {
            LoanApplication loanApplication = new LoanApplication();
            loanApplication.LoanAmount = 2000;
            loanApplication.LoanPeriod = 40;
            loanApplication.PersonalCode = "49002010987";
            CreditScore cs = _loanService.ApplyForLoan(loanApplication);

            CreditScore expectedCreditScore = new CreditScore();
            expectedCreditScore.MaxLoanAmount = 0;
            expectedCreditScore.Score = 0;

            Assert.AreEqual(40, cs.LoanPeriod);
            Assert.AreEqual(10000, cs.MaxLoanAmount);
            Assert.AreEqual(true, cs.CanIssueLoan);
        }

        [Test]
        public void Get_Scoring_For_Over_Limit_Loan()
        {
            LoanApplication loanApplication = new LoanApplication();
            loanApplication.LoanAmount = 11000;
            loanApplication.LoanPeriod = 40;
            loanApplication.PersonalCode = "49002010998";
            CreditScore cs = _loanService.ApplyForLoan(loanApplication);

            CreditScore expectedCreditScore = new CreditScore();
            expectedCreditScore.MaxLoanAmount = 0;
            expectedCreditScore.Score = 0;

            Assert.AreEqual(40, cs.LoanPeriod);
            Assert.AreEqual(10000, cs.MaxLoanAmount);
            Assert.AreEqual(true, cs.CanIssueLoan);
        }
    }
}