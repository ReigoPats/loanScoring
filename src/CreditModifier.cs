namespace loanScoring.Credit;

public enum CreditModifier : int
{
    Debt = 0,
    Segment1 = 100,
    Segment2 = 300,
    Segment3 = 1000
}