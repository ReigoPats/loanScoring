import React, { Component } from 'react';
import { FormErrors } from './FormErrors';

const minimumLoanPeriod = 12;
const maximumLoanPeriod = 60;

const minimumLoanAmount = 2000;
const maximumLoanAmount = 10000;

export default class ApplyLoan extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loanAmount: 2000,
      loanPeriod: 12,
      personalCode: '',
      canIssueLoan: 'No',
      maxLoanAmount: 0,
      finalLoanPeriod: 0,
      applicationErrors: {loanAmount: '', loanPeriod: '', personalCode: ''},
      applicationIsValid: false
    };

    this.handleChangeLoanAmount = this.handleChangeLoanAmount.bind(this);
    this.handleClickApplyLoan = this.handleClickApplyLoan.bind(this);
    this.handleChangeLoanPeriod = this.handleChangeLoanPeriod.bind(this);
    this.handleChangePersonalCode = this.handleChangePersonalCode.bind(this);
    this.getLoanApplicationResult = this.getLoanApplicationResult.bind(this);

  }

  async getLoanApplicationResult() {
    this.validateLoanApplication();
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ 
          LoanAmount: this.state.loanAmount,
          LoanPeriod: this.state.loanPeriod,
          PersonalCode: this.state.personalCode
         })
    };

    fetch('/loan', requestOptions)
        .then(response => response.json())
        .then(data => {
          this.setState({ 
            canIssueLoan: data.canIssueLoan? 'Yes': 'No', 
            maxLoanAmount: data.maxLoanAmount,
            finalLoanPeriod: data.loanPeriod
           });
           console.log('#data#####', data)
        })
        .catch((error) => {
          console.log(error);
          return Promise.reject();
        });;

  }

  handleChangeLoanPeriod(event) {
    this.setState({loanPeriod: event.target.value}, this.validateLoanPeriod(event.target.value));
  }

  handleChangePersonalCode(event) {
    let newVal = event.target.value.trim();
    this.setState({personalCode: newVal}, this.validatePersonalCode(newVal));
  }

  handleChangeLoanAmount(event) {
    this.setState({loanAmount: event.target.value}, this.validateLoanAmount(event.target.value));
  }

  handleClickApplyLoan(event) {
    this.getLoanApplicationResult();
    event.preventDefault();
  }

  validateLoanApplication(){
    this.validatePersonalCode(this.state.personalCode);
    this.validateLoanPeriod(this.state.loanPeriod);
    this.validateLoanAmount(this.state.loanAmount);
  }

  validatePersonalCode(personalCode) {
    let applicationValidationErrors = this.state.applicationErrors;
    if (personalCode == '') applicationValidationErrors.personalCode = 'cannot be empty';
    else applicationValidationErrors.personalCode = '';
    this.setState({applicationErrors: applicationValidationErrors});
    this.isApplicationValid();
  }

  validateLoanPeriod(loanPeriod) {
    let applicationValidationErrors = this.state.applicationErrors;
    if (loanPeriod < minimumLoanPeriod) applicationValidationErrors.loanPeriod = 'is too small. Minimum is ' + minimumLoanPeriod.toString();
    else if (loanPeriod > maximumLoanPeriod) applicationValidationErrors.loanPeriod = 'is too big. Maximum is ' + maximumLoanPeriod.toString();
    else applicationValidationErrors.loanPeriod = '';

    this.setState({applicationErrors: applicationValidationErrors});
    this.isApplicationValid();
  }

  validateLoanAmount(loanAmount) {
    let applicationValidationErrors = this.state.applicationErrors;
    if (loanAmount < minimumLoanAmount) applicationValidationErrors.loanAmount = 'is too small. Minimum is ' + minimumLoanAmount.toString();
    else if (loanAmount > maximumLoanAmount) applicationValidationErrors.loanAmount = 'is too big. Maximum is ' + maximumLoanAmount.toString();
    else applicationValidationErrors.loanAmount = '';

    this.setState({applicationErrors: applicationValidationErrors});
    this.isApplicationValid();
  }

  isApplicationValid(){
    let isValid = this.state.applicationErrors.loanAmount === '' && this.state.applicationErrors.personalCode === '' && this.state.applicationErrors.loanPeriod === '';
    this.setState({applicationIsValid: isValid});
  }

  render() {
    return (
      <div>
        <div className="panel panel-default">
          <FormErrors formErrors={this.state.applicationErrors} />
        </div>
        <div>
          <h1>Apply for the loan:</h1>
            <label>
              Personal Code:
              <input type="text" value={this.state.personalCode.value} onChange={this.handleChangePersonalCode} />
            </label><br />
            <label>
              Loan Amount:
              <input type="text" defaultValue={minimumLoanAmount} value={this.state.loanAmount.value} onChange={this.handleChangeLoanAmount} />
            </label><br />
            <label>
              Loan Period (months):
              <input type="text" defaultValue={minimumLoanPeriod} value={this.state.loanPeriod.value} onChange={this.handleChangeLoanPeriod} />
            </label><br />
            <button className="btn btn-primary" disabled={!this.state.applicationIsValid} onClick={this.handleClickApplyLoan}>Apply</button>
        </div>
        <div>
          <h1>Your Application Result:</h1><br />
            <label><b>Application Accepted:</b> {this.state.canIssueLoan} </label><br />
            <label><b>Your requested amount:</b> {this.state.loanAmount} </label><br />
            <label><b>Amount of loan we can give:</b> {this.state.maxLoanAmount} </label><br />
            <label><b>Your final loan period:</b> {this.state.finalLoanPeriod} </label><br />
        </div>
      </div>
    );
  }
}
