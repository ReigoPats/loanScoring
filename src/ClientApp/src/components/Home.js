import React, { Component } from 'react';
import ApplyLoan from './ApplyLoan.js';

export class Home extends Component {
  static displayName = Home.name;

  render() {
    return (
      <div>
        <ApplyLoan />
        <div>
          <h2>test data:</h2>
          49002010965 - debt (credit_modifier = 0)<br/>
          49002010976 - segment 1 (credit_modifier = 100)<br/>
          49002010987 - segment 2 (credit_modifier = 300)<br/>
          49002010998 - segment 3 (credit_modifier = 1000)<br/>
          all others - segment 3 (credit_modifier = 1000)<br/>
        </div>
      </div>
    );
  }
}
