namespace loanScoring.Credit;
using loanScoring.Settings;

public class CreditScore {

    public CreditScore() {
        this.Score = 0;
    }

    public float MaxLoanAmount {get; set;}
    public float LoanPeriod {get; set;}
    public float Score {get; set;} = 0;
    public bool CanIssueLoan {get {return this.Score >= LoanModule.CreditScoreTreshold; }}
}