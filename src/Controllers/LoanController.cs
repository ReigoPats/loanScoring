﻿using Microsoft.AspNetCore.Mvc;
using loanScoring.Settings;
using loanScoring.Credit;
using loanScoring.LoanService;
namespace loanScoring.Controllers;

[ApiController]
[Route("[controller]")]
public class LoanController : ControllerBase
{

    private readonly ILogger<LoanController> _logger;
    private readonly ILoanService _loanService;

    public LoanController(ILogger<LoanController> logger, ILoanService loanService)
    {
        _logger = logger;
        _loanService = loanService;
    }

    [HttpPost]
    public ActionResult<LoanApplicationResponse> Post([FromBody]LoanApplicationRequest request)
    {
        List<string> validationResult = validateLoanApplicationInput(request);
        var result = new LoanApplicationResponse();
        if (!validationResult.Any()) {
            LoanApplication loanApplication = new LoanApplication();

            // here we should think of using automapper
            loanApplication.LoanAmount = request.LoanAmount;
            loanApplication.LoanPeriod= request.LoanPeriod;
            loanApplication.PersonalCode = request.PersonalCode;

            var loanApplicationResult = _loanService.ApplyForLoan(loanApplication);

            // here we should think using automapper
            result.CanIssueLoan = loanApplicationResult.CanIssueLoan;
            result.MaxLoanAmount = loanApplicationResult.MaxLoanAmount;
            result.LoanPeriod = loanApplicationResult.LoanPeriod;
        } else {
            result.validationErros = validationResult;
        }
        
        return result;
    }

    private List<string> validateLoanApplicationInput (LoanApplicationRequest request) {
        List<string> inputDataErrors = new List<string>();
        if (request.LoanAmount < LoanModule.MinimumLoanAmount) inputDataErrors.Add("LoanAmount too small");
        if (request.LoanAmount > LoanModule.MaximumLoanAmount) inputDataErrors.Add("LoanAmount exceeds maximum loan amount");

        if (request.LoanPeriod < LoanModule.MinimumLoanPeriod) inputDataErrors.Add("LoanPeriod too small");
        if (request.LoanPeriod > LoanModule.MaximumLoanPeriod) inputDataErrors.Add("LoanPeriod exceeds maximum loan period");
        return inputDataErrors;
    }
}
