namespace loanScoring;

public class LoanApplicationResponse
{
   public LoanApplicationResponse() {
      validationErros = new List<string>();
   }

   public bool CanIssueLoan {get; set;}
   public float loanAmount {get; set;}
   public float LoanPeriod {get; set;}
   public float MaxLoanAmount {get; set;}
   public List<string> validationErros {get; set;}
}
