namespace loanScoring;

public class LoanApplicationRequest
{   
   public float LoanAmount {get; set;}
   public float LoanPeriod {get; set;}
   public string? PersonalCode {get; set;}
}
