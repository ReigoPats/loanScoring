namespace loanScoring.Settings;

public static class LoanModule {
    public const float MinimumLoanAmount = 2000;
    public const float MaximumLoanAmount = 10000;
    public const float  MinimumLoanPeriod = 12;
    public const float MaximumLoanPeriod = 60;
    public const float CreditScoreTreshold = 1;
}
