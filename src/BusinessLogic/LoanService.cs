namespace loanScoring.LoanService;
using loanScoring.Credit;
using loanScoring.Settings;

public class LoanService : ILoanService {
    public LoanService () {}

    public CreditScore ApplyForLoan (LoanApplication loanApplication) {
        CreditModifier creditModifier = GetCreditModifier(loanApplication.PersonalCode);
        CreditScore creditScoroing = GetCreditScoring((int)creditModifier, loanApplication.LoanAmount, loanApplication.LoanPeriod);
        return creditScoroing;
    }

     public CreditScore GetCreditScoring (float creditModifier, float loanAmount, float loanPeriod) {
        CreditScore cs = new CreditScore();
        float period = loanPeriod;

       // if (loanAmount > LoanModule.MaximumLoanAmount) loanAmount = LoanModule.MaximumLoanAmount;

        float score = CalculateCreditScore(creditModifier, loanAmount, period);

        if (score >= LoanModule.CreditScoreTreshold) {
            loanAmount = FindMaximumLoanAmount(creditModifier, loanAmount, period);
        } 
        else if (score == 0){
            loanAmount = 0;
        } else {
            float newLoanAmount = FindLowerLoanAmount(creditModifier, loanAmount, period);
            if (newLoanAmount >= LoanModule.MinimumLoanAmount) {
                loanAmount = newLoanAmount;
                score = CalculateCreditScore(creditModifier, loanAmount, period);
            } else {
                float newPeriod = FindLongerPeriod(creditModifier, loanAmount, period);
                if (newPeriod >= LoanModule.MinimumLoanPeriod && newPeriod <= LoanModule.MaximumLoanPeriod){
                    period = newPeriod;
                    score = CalculateCreditScore(creditModifier, loanAmount, period);
                } else
                {
                    loanAmount = 0;
                }
            }
        }
        
        cs.LoanPeriod = period;
        cs.MaxLoanAmount = loanAmount;
        cs.Score = score;
        
        return cs;
    }

    private float CalculateCreditScore (float creditModifier, float loanAmount, float period) {
        return (creditModifier / loanAmount) * period;
    }
    
    private float FindMaximumLoanAmount (float creditModifier, float loanAmount, float period) {
        float score = 0;
        while (loanAmount >= LoanModule.MinimumLoanAmount)
        {
            loanAmount ++;
            score = CalculateCreditScore(creditModifier, loanAmount, period);
            if (score < LoanModule.CreditScoreTreshold || loanAmount > LoanModule.MaximumLoanAmount) {
                loanAmount--;
                break;
            }
        }
        return loanAmount;
    }
    private float FindLowerLoanAmount (float creditModifier, float loanAmount, float period) {
        float score = 0;
        while (loanAmount >= LoanModule.MinimumLoanAmount)
        {
            loanAmount --;
            score = CalculateCreditScore(creditModifier, loanAmount, period);
            if (score >= LoanModule.CreditScoreTreshold) {
                return loanAmount;
            }
        }
        return 0;
    }
    private float FindLongerPeriod (float creditModifier, float loanAmount, float period) {
        float score = 0;
        while (period <= LoanModule.MaximumLoanPeriod)
        {
            score = CalculateCreditScore(creditModifier, loanAmount, period);
            if (score >= LoanModule.CreditScoreTreshold) {
                return period;
            }
            period ++;
        }
        return 0;
    }

    public CreditModifier GetCreditModifier (string personalCode) {
        if (personalCode == "49002010965") return CreditModifier.Debt;
        else if (personalCode == "49002010976") return CreditModifier.Segment1;
        else if (personalCode == "49002010987") return CreditModifier.Segment2;
         
        return CreditModifier.Segment3;
    }
}