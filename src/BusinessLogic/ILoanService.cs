namespace loanScoring.LoanService;
using loanScoring.Credit;

public interface ILoanService {
    CreditScore ApplyForLoan (LoanApplication loanApplication);
}