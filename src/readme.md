To run this project:
open with visual Code or visual studio and Run-> Start with or with out debugging.



Thoughts about that project

Decided to use React as Frontend and C# 6 as backend. React because i have used it really little and wanted to learn more. 
C# 6 also the same reasons. I have been working a lot on older monoliths so it’s good to see what newer technology is bringing.

Used dotnet core react template to build this project. It automatically creates ClientApp and Api projects. 
Due that it means that project contains some additional overhead what is not exactly needed for this small project. 
But the overhead is not making project messy so i did not root it out.

About project structure.

Template already separated ClientApp (react front end) into separate folder so i did not make it as separate project. 
In real life project i would separate them at least into separate projects and maybe even into separate repositories. 
All depends what CD/CI is been used and how dev teams are working.

Under Controllers there is api controller and idea is that controller does not have any business logic it only validates input/output. 
In some project i have made separate DTO project so other services can easily include them if they want to communicate with each other. 
Currently there is only 2 DTO. Under Business Logic i added BL layer so all services etc. would be there. 
Currently there is only 1 service. Maybe it can be spitted into multiple services as loan, creditscroring and maybe something else but i prefer to separate things when there is enough functions/object to into it. 

Did not move creditModifier asking to separate api. Maybe it would have simulated then real world better. But currently for me making additional api project that would return basically hardcoded values are over kill. And also, assignment task did not require it. 

Used Dependence Injection to inject loanSerivce into loan controller. Simplifies life. 
To make life more easier then there is possibility to use automappers to map information between diffrent layers like: business Object to DTO and vice-versa. 
Currently there is only one small controller so setting it up would be bigger job than adding few mapping lines and it would make understanding more complex.

For testing i used Nunit testing framework. One test that fails points out good point about when should we do validations:
in client side? in controller? in service/engine?

Currently i'm doing validation in client side only for client UX. There must always be checks in BE side for security sake.
But doing checks in controller means that if other service is using service that checks are in controller then it may use it wrongly.
But again if doing checks in deeper levels then it makes system more complicated because you will need to implement error propagation.
Ideally in real project validateLoanApplicationInput should validate only if something is missing not correctnes for business. 
Business validation checks should be implemented in business layer but this project is so small and straight forward then there is no
point to separate this and build error propagation.
